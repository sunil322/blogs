---
title: Pass by Value and Pass by Reference in JavaScript
date: 2023-03-02 14:41:24
tags: 
---

In this post, we will learn about pass-by-value and pass-by-reference in JavaScript.

## Pass by Value
In pass-by-value, the function is called by passing the value of the variable as arguments. If the value of the arguments changes inside the function, then it will not affect the original value of the variable.

In JavaScript, all primitive data types follow pass-by-value.

```js
function changeValue(number){
    number = 10;
    //Inside method 
    console.log(number); //10
}

let number = 5;
//Before method call
console.log(number); // 5

changeValue(number);

//After method called
console.log(number); //5
```


* In the above example, the value of the number variable outside the function is 5.
* But anywhere inside the function, the value will be 10.
* One copy of the number variable is created inside the `changeValue` function and all the changes inside the function we made that change applicable to that copy only, will not affect the original value of the variable.

## Pass by Reference
In pass-by-reference, the function is called by passing the reference or address of the variable as arguments. Any changes to the arguments inside the function will affect the original value of the variable.

In JavaScript, arrays and objects follow pass-by-reference.


Example with object
```js
function changeObject(student){
    student.name ="Shyam";
}

let student = {
    name:"Ram",
    age:25
}
//Before method call
console.log(student.name); //Ram

changeObject(student);

//After method call
console.log(student.name); //Shyam
```

Example with array
```js
function changeArray(listOfNumber){
    listOfNumber[0] = 19;;
}

let listOfNumber = [5,10];

//Before method call
console.log(listOfNumber[0]); // 5

changeArray(listOfNumber);

//After method call
console.log(listOfNumber[0]); //19
```
* In the object example, when we try to change the name property of the student object, it also changed the original student object.
* Same with an array, in the method, we change the value for 0 index, this change also happens to the original array.
* In an object, if we try to perform changes through property inside a function then it will also change the original object as we saw earlier.

There is one interesting case in arrays and objects. Let's see that through the following code snippets.

* If we try to change the object value directly, then it will not affect the original object. Once we exit from the function we will get the original value. It works like a pass-by-value.
```js
function changeObject(student){
    student = {
        age : 29
    }
}
let student = {
    name :"Ranjeet",
    age : 25
}

console.log(student.age);//25
changeObject(student);
console.log(student.age);//25
```
* In arrays, the same rule follows as the object, if we try to change the array through the index inside a function, then the content of the original array will be also changed.
* But if we try to change the array directly, then the content of the original array will not be changed.
  
```js
function changeArray(listOfNumber){
    listOfNumber = [20,30];
}

let listOfNumber = [5,10];

//Before method call
console.log(listOfNumber[0]); // 5

changeArray(listOfNumber);

//After method call
console.log(listOfNumber[0]); //5

```
### Sometimes our requirements might be different. When we want to make some changes in the array or object without modifying the original content.
Let's see how to achieve these things using the spread operator.
## Spread Operator
The syntax is three dots (...) followed by the array or object, it expands the array into individual elements and expands the object into the individual property. So it can be used where zero or more arguments (for function calls) or elements (for array literals) are expected.

### How to pass multiple arguments to the function using the spread operator.
```js
function add(number1,number2,number3){
    return number1+number2+number3;
}

let numbers = [10,15,18];

console.log(add(...numbers));
```
output
```js
43
```
* In this example, when we pass the `numbers` array followed by the spread operator as arguments to `add` function, it converts the array into individual elements.
* It works like we pass three different arguments to `add` function.

### How to add some new elements to the array without modifying the original array content using the spread operator?
```js
function addNewValue(numbers){
    return numbers = [...numbers,10,15];
}

let numbers = [2,5,7,8];

console.log(addNewValue(numbers));

console.log(numbers);
```
output
```js
[ 2, 5, 7, 8, 10, 15 ]
[ 2, 5, 7, 8 ]
```
* In this example, we expand the `numbers` array into individual elements and add two new values inside `addNewValue` function which will create a new array without modifying the original array.

### How to add the new key to the object without modifying the original object?
```js
function changeEmployeeData(employee){
    return employee = {...employee,salary:30000};
}

let employee = {
    name:"Sandeep",
    age:25
};

console.log(changeEmployeeData(employee));

console.log(employee);
```
output
```js
{ name: 'Sandeep', age: 25, salary: 30000 }
{ name: 'Sandeep', age: 25 }
```
* In this example, through the spread operator, we copy all the properties of the employee object and add a new `salary` property inside the `changeEmployeeData` function.
* As we are not changing the value through the property reference of the object, the changes will not be reflected in the original object.