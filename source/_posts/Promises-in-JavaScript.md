---
title: Promises in JavaScript
date: 2023-03-14 17:29:59
tags:
---
# Promises methods in JavaScript
In this blog, we will learn about different methods of promises.

## Promise.all()
* The `Promise.all()` method takes an iterable of promises as input and returns a single Promise. This return promise will be fulfilled when all the promises inside that iterable will be fulfilled, if any of the promises reject, then that returned promise will also be rejected.
* The single promise that is returned, if all the promises resolve then returns the array of fulfillment values. If any promise rejects that returns the reason for which the first promise fails.

Let's see through this example if all promises will be resolved.
```js
const promise1 = 1;

const promise2 = new Promise((resolve,reject)=>{
    setTimeout(() => {
        resolve(10);
      }, 2* 1000);
});

const promise3 = new Promise((resolve,reject)=>{
    setTimeout(() => {
        resolve(100);
      }, 1* 1000);
});

Promise.all([promise1, promise2, promise3]).then((results)=>{
    console.log(results);
})
.catch((error)=>{
    console.log(error);
})
  

```
output
```js
[ 1, 10, 100 ]
```
* It gives the resolve values for all promises in the form of an array.
  
Let's see through this example if any promise got rejected.
```js
const promise1 = 1;

const promise2 = new Promise((resolve,reject)=>{
    setTimeout(() => {
        reject("rejected");
      }, 2* 1000);
});

const promise3 = new Promise((resolve,reject)=>{
    setTimeout(() => {
        resolve(100);
      }, 1* 1000);
});

Promise.all([promise1, promise2, promise3]).then((values)=>{
    console.log(values);
})
.catch((error)=>{
    console.log(error);
})
  
```
output
```js
rejected
```
* In this above example, as soon as `promise2` got rejected it returns the reject reason for `promise2` which we passed as arguments for reject.

## Promise.allSettled()
* The `Promise.allSettled()` method takes an iterable of promises as input and returns a single Promise. This method waits for all promises to be settled i.e either resolved or rejected.
* when all promises in the given iterable have settled it returns fulfillment value as an array of objects.

Each outcome object has the following properties.
* `status`  a string either "fulfilled" or "rejected". It represents the eventual state of the promise.
* `value` only present if the status is fulfilled. It contains the value that was passed in resolve.
* `reason` only present if the status is rejected. It contains the value that was passed in reject.
```js
const promise1 = new Promise((resolve,reject) =>{
    setTimeout(()=>{
        resolve(10);
    },1 * 1000);
});

const promise2 = new Promise((resolve,reject) =>{
    setTimeout(()=>{
        reject(20);
    },1 * 1000);
});

const promises = [promise1, promise2];

Promise.allSettled(promises)
.then((results)=>{
    console.log(results);
})
.catch((error)=>{
    console.log(error);
})
```
output
```js
[
  { status: 'fulfilled', value: 10 },
  { status: 'rejected', reason: 20 }
]
```
## Promise.any()
* The `Promise.any()` method takes an iterable of promises as input and returns a single Promise. 
* This returned promise is fulfilled when any of the promises are resolved. It rejects when all of the promises reject.
```js
const promise1 = new Promise((resolve,reject) =>{
    setTimeout(()=>{
        resolve(10);
    },1 * 2000);
});

const promise2 = new Promise((resolve,reject) =>{
    setTimeout(()=>{
        resolve(20);
    },1 * 1000);
});

const promises = [promise1, promise2];

Promise.any(promises)
.then((results)=>{
    console.log(results);
})
.catch((error)=>{
    console.log(error);
})
```
```js
20
```
* `promise1` will wait for 2 seconds (2000 milliseconds) and `promise2` will wait for 1 second to resolve. So `promise2` got resolved first, and as soon as `promise2` got resolved `Promise.any()` method returns the fulfillment value of `promise2`
* It will not wait for `promise1`.
## Promise.race()
* The `Promise.race()` method takes an iterable of promises as input and returns a single Promise. This returned promise settles with the eventual state of the first promise that settles.
* It waits as soon as one promise got resolved or rejected it will execute.

```js
const promise1 = new Promise((resolve,reject) =>{
    setTimeout(()=>{
        reject(10);
    },1 * 1000);
});

const promise2 = new Promise((resolve,reject) =>{
    setTimeout(()=>{
        resolve(20);
    },1 * 2000);
});

const promises = [promise1, promise2];

Promise.race(promises)
.then((results)=>{
    console.log(results);
})
.catch((error)=>{
    console.log(error);
});
```
output
```js
10
``` 
* `promise1` will wait for 1 second for reject and `promise2` will wait for 2 seconds for resolve.
* So `promise1` will be rejected first, as soon as `promise1` rejected `Promise.race()` method returns the rejection value of `promise1`.