---
title: Sort in JavaScript
date: 2023-03-06 15:55:16
tags:
---

# Sorting in JavaScript
The JavaScript **sort( )** method sorts the elements of the array.
* When we apply the **sort( )** method in an array, the original array will sort. 
* The **sort( )** method returns a reference to the original array.
```js
let pets = ["dog","cat","cow"];
let sortedData = pets.sort();
console.log(pets);
console.log(sortedData);
console.log(pets === sortedData);
```
output
```js
[ 'cat', 'cow', 'dog' ]
[ 'cat', 'cow', 'dog' ]
true
```
* In the above example, `sortedData` is a reference to the `pets` array.

This method is only applicable to arrays. So the elements which we want to sort must be inside an array. Otherwise, we have to convert to the array of elements for using this method.
### Syntax
```js
array.sort(compareFunction);
```
* Passing a parameter to the sort function is optional, if we didn't pass any parameter, by default it sorts the array in ascending order. It converts the element into strings and sorts by comparing the sequences of UTF-16 code values.
* We can pass compare function for sorting the array in our own defined order.

## Compare Function
The compare function is used for defining a sorting order. We have to pass compare function to the sort method.

We can write our sorting logic in compare function based on our requirements. The compare function at a time compares two elements of the array.
### Syntax
```
function(firstElement,secondElement){
    // code
}

function compareFunction(firstElement,secondElement){
    //code
}

(firstElement,secondElement)=>{
   //code
}
```
* 1. If the compare function return value is negative, `firstElement` will sort before `secondElement`.
* 2. If the compare function return value is positive, `firstElement` will sort after `secondElement`.
* 3. If the compare function return value is zero, `firstElement` and `secondElement` order will not change.
## Sort Array of String
Let's see through this example how the `sort()` method works for string elements.
#### Sorting Alphabetically
Example 1
```js
let pets = ["dog","cat","cow"];
pets.sort();
console.log(pets);
```
output
```js
[ 'cat', 'cow', 'dog' ]
```
Example 2
```js
let name = ["Shubham","anil","Ranjeet","bhabesh"];
name.sort();
console.log(name);
```
output
```js
[ 'Ranjeet', 'Shubham', 'anil', 'bhabesh' ]
```
* In example 1, the output comes correctly, as all the elements are in the same case.
* But in example 2, the elements are in different cases, if we see the output it sorts the element according to the case.
* In example 1 and example 2 we didn't pass any compare function to the sort method. So we have to rely on the default sorting of sort method.
* For sorting by ignoring cases. We have to pass compare function. We can write compare function like this.
  
Example 3
```js
let names = ["Shubham","anil","Ranjeet","bhabesh"];
names.sort(function(name1,name2){
    return name1.toLowerCase().localeCompare(name2.toLowerCase());
});
console.log(names);
```
output 
```js
[ 'anil', 'bhabesh', 'Ranjeet', 'Shubham' ]
```
* In example 3, we passed a function to the sort method, in that function we convert the elements to lowercase and then compare the elements and sort according to the compare result.
#### Sorting Reverse Alphabetically
Example 1
```js
let pets = ["dog","cat","cow"];
pets.sort();
pets.reverse();
console.log(pets);
```
output
```
[ 'dog', 'cow', 'cat' ]
```
Example 2
```js
let names = ["Shubham","anil","Ranjeet","bhabesh"];
names.sort(function(name1,name2){
    return name2.toLowerCase().localeCompare(name1.toLowerCase());
});
console.log(names);
```
output
```js
[ 'Shubham', 'Ranjeet', 'bhabesh', 'anil' ]
```
* In example 1, we call the reverse method on a sorted array, and we get the reverse alphabetical order.
* In example 2, for getting the reverse alphabetical order, we only change the return value in compare function.
## Sort Array of Integers
* By default, the sort method sorts the elements as strings.
* For sorting the integer elements we have to pass compare function to the sort method.

Let's see through this example how the `sort()` method works for Integer elements.
#### Ascending Order
```js
let numberList = [5,4,18,15,20];
numberList.sort(function(number1,number2){
   return number1-number2;
});
console.log(numberList);
```
output
```js
[ 4, 5, 15, 18, 20 ]
```
#### Descending Order
```js
let numberList = [5,4,18,15,20];
numberList.sort(function(number1,number2){
   return number2-number1;
});
console.log(numberList);
```
output
```js
[ 20, 18, 15 ,5 ,4 ]
```
* In ascending order, we subtract the `number2` from the `number1`, and then it will sort the elements based on return values, which we already see in the compare function section how it works.
* In descending order, we only change the return value by changing the variable order. 
## Sort Array of Floating Point Numbers
Let's see through this example how the `sort()` method works for the floating point number elements.
#### Ascending Order
```js
let numberList = [5.5,4.3,18.2,15.1,2.3];
numberList.sort(function(number1,number2){
   return number1-number2;
});
console.log(numberList);
```
output
```js
[ 2.3, 4.3, 5.5, 15.1, 18.2 ]
```
#### Descending Order
```js
let numberList = [5.5,4.3,18.2,15.1,2.3];
numberList.sort(function(number1,number2){
   return number2-number1;
});
console.log(numberList);
```
output
```js
[ 18.2, 15.1, 5.5, 4.3, 2.3 ]
```

* The sort method works the same as integers for floating point numbers, also no changes are required in compare function implementation.
## Sort Array Of Objects
For sorting an array of objects, we have to pass the compare function to the sort method. By using any property, we can sort the objects. 

In this example, we will use `salary` to sort the `employees` objects.

Let's see through this example how the `sort()` method works for the object.

#### Ascending order
```js
let employees = [
    {
        name: "Nirbhay",
        salary: 30000
    },
    {
        name: "Ranjeet",
        salary: 25000
    },
    {
        name: 'ShaktiRaj',
        salary: 35000
    }
];
employees.sort(function(employeeData1,employeeData2){
    return employeeData1.salary-employeeData2.salary;
});
console.log(employees);
```
output
```js
[
  {
    name: 'Ranjeet',
    salary: 25000
  },
  {
    name: 'Nirbhay',
    salary: 30000
  },
  {
    name: 'ShaktiRaj',
    salary: 35000 
  }
]
```
#### Descending Order
```js
let employees = [
    {
        name: "Nirbhay",
        salary: 30000
    },
    {
        name: "Ranjeet",
        salary: 25000
    },
    {
        name: 'ShaktiRaj',
        salary: 35000
    }
];
employees.sort(function(employeeData1,employeeData2){
    return employeeData2.salary-employeeData1.salary;
});
console.log(employees);
```
output
```js
[
    { 
        name: 'ShaktiRaj', 
        salary: 35000 
    },
    { 
        name: 'Nirbhay', 
        salary: 30000 
    },
    { 
        name: 'Ranjeet', 
        salary: 25000 
    }
]
```
* In the above examples, we passed a compare function where the salary of all employees will compare (salary of two employees at a time) and based on the return value the objects will sort.
## Sort Array Of Objects Using Multiple keys
We can sort the objects also by using multiple keys.

* In this example, in the `cricketers` array, every object has two properties `firstName` and `lastName`.
* First, we will sort using `firstName`, if `firstName` matches, then we will sort using `lastName`.
  
Let's see through this example how to use the `sort()` method with multiple keys of objects.
```js
let cricketers = [
    {
        firstName: "Virat",
        lastName: "Kohli"
    },
    {
        firstName: "Rohit",
        lastName:"Sharma"
    },
    {
        firstName: "Rahul",
        lastName: "Tripathi"
    },
    {
        firstName: "Rahul",
        lastName: "Dravid"
    },
    {
        firstName: "Axar",
        lastName: "Patel"
    }
];
cricketers.sort((cricketer1,cricketer2)=>{
    if(cricketer1.firstName > cricketer2.firstName){
        return 1;
    }else if(cricketer1.firstName < cricketer2.firstName){
        return -1;
    }else{
        if(cricketer1.lastName > cricketer2.lastName){
            return 1;
        }else if(cricketer1.lastName < cricketer2.lastName){
            return -1
        }else{
            return 0;
        }
    }
});
console.log(cricketers);
```
output
```js
[
  { 
    firstName: 'Axar', 
    lastName: 'Patel' 
  },
  { 
    firstName: 'Rahul',
    lastName: 'Dravid'
  },
  { 
    firstName: 'Rahul', 
    lastName: 'Tripathi' 
  },
  { 
    firstName: 'Rohit', 
    lastName: 'Sharma' 
  },
  { 
    firstName: 'Virat', 
    lastName: 'Kohli' 
  }
]
```