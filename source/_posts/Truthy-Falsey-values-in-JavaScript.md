---
title: Truthy and Falsey values in JavaScript
date: 2023-03-02 14:27:20
tags: 
---

In this blog, we will learn about truthy and falsey values in JavaScript.

Before learning about truthy and falsey values we need to know about the **type coercion**. It means the conversion of values from one data type to another implicitly. JavaScript uses **type coercion** in the boolean context.

Let's understand through the following code snippet. 
```
let firstValue = 5;
let secondValue = 0;
// As value of firstValue variable is 5 it will be consider true. So the code inside if block will be execute.
if(firstValue){
    console.log("Inside block"); //Inside block
}

//As value of secondValue varibale is 0 it will be consider false. So if block will not execute.
if(secondValue){
    console.log("Inside block"); //not reachable
}
```
## Truthy Values
* If a value is evaluated as true in the boolean context, then that value is known as a truthy value.
* Examples of some truthy values are "text", true, 5 etc.
* JavaScript uses **type coercion** in the boolean context for evaluating whether the value is truthy or falsey.
* The followings are some examples of truthy values.
  
  ```js
  if(true){
    console.log("Inside block"); //Inside block
  }
  if(5){
    console.log("Inside block"); //Inside block
  }
  if(10.3){
    console.log("Inside block"); //Inside block
  }
  if(10n){
    console.log("Inside block"); //Inside block
  }
  if("text"){
    console.log("Inside block"); //Inside block
  }
  if(" "){
    console.log("Inside block"); //Inside block
  }
  ```

## Falsey Values
* If a value is evaluated as false in the boolean context, then that value is known as a falsey value.
* These are the falsey values 0, false, -0, null, undefined, NaN, "", '', ``.
* The followings are some examples of falsey values.
  ```js
  if(false){
    console.log("value is truthy");
  }else{
    console.log("Value is falsey"); //Value is falsey
  }
  
  if(0){
    console.log("value is truthy"); 
  }else{
    console.log("Value is falsey"); //Value is falsey
  } 

  if(""){
    console.log("value is truthy"); 
  }else{
    console.log("Value is falsey"); //Value is falsey
  }

  if(null){
    console.log("value is truthy"); 
  }else{
    console.log("Value is falsey"); //Value is falsey
  }

  if(NaN){
    console.log("value is truthy"); 
  }else{
    console.log("Value is falsey"); //Value is falsey
  }

  if(undefined){
    console.log("value is truthy"); 
  }else{
    console.log("Value is falsey"); //Value is falsey
  }
  ```